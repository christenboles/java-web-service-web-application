Application can be found online here: 

[http://web-place.info/Json_to_mysql_webservice/](http://web-place.info/Json_to_mysql_webservice/)

Description: Java (Java Server Faces/MySQL) web application which connects to web service and sends mail message if email of user is present. Connection to JSON web service and saving JSON data and html form data into database. Maximum number of valid request/inserts per hour by user (based on ip address): 1

### How do I get set up? ###

* Apache Tomcat 7.0, JDK 1.7, Dynamic web module: 3.0, JavaServer Faces 2.2
* MySQL 5.6.14
* Deployment via .war file