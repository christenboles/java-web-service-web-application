package com.dev.user.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DatabaseManager {

    public Connection connection;
    public Statement statement;
    public ResultSet rs;
    public PreparedStatement prepared_statement;
	private PreparedStatement ps;
	private Statement st;
    public String sql;
    
    private static String _JDBC_DRIVER;
    private static String _USERNAME;
    private static String _PASSWORD;
    private String _DATABASE;
    private String _CONNECTION_STRING;
    
    private String sql_command;
    private String content;
    
    public DatabaseManager(){
        connection = null;
        statement = null;
        rs = null;
        sql = null;
        _JDBC_DRIVER = "com.mysql.jdbc.Driver";
        _USERNAME = "root";
        _PASSWORD = "";
        _DATABASE = "record";
        _CONNECTION_STRING = "jdbc:mysql://localhost:3306/"+_DATABASE+"?useUnicode=true&characterEncoding=UTF-8";
        
        /*try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			System.out.println("DriverManager nije registrirao driver za JDBC");
			e1.printStackTrace();
		}*/
        
        System.out.println("Testiranje spoja na MySQL...");
        try {
            Class.forName(_JDBC_DRIVER);
            System.out.println("Driver za MySQL "+_JDBC_DRIVER+" je registrian i konekcije su omogućene ");
        } catch (ClassNotFoundException e){
            System.out.println("Ne postoji JDBC driver za MySQL na serveru ili vašem računalu.");
            e.printStackTrace();
        }
    }
    
    public Connection getConnection(){
        
        connection = null;
        
        try {
        	connection = DriverManager.getConnection(_CONNECTION_STRING, _USERNAME, _PASSWORD);
        } catch (SQLException e){
            System.out.println("Konekcija se nije izvršila: provjerite output konzolu.");
        } finally {
            if(connection != null){
                System.out.println("Konekcija na bazu je uspješno ostvarena.");
            } else {
                System.out.println("Konekcija na bazu se nije izvršila: provjerite da li je vaša baza uopće postoji.");
            }
        }

        return connection;
        
    }
    
	public int save_items(String table, int table_id, String sql) throws SQLException{
		String table_dod = "";
		String sql_dod = "";
		String sql_dod2 = "";
		if(table_id == 0)
		{
			table_dod = "insert into";
			sql_dod = "";
			sql_dod2 = ", created = now() ";
		}
		else 
		{
			table_dod = "update";
			sql_dod = " where id = '"+table_id+"' ";
			sql_dod2 = "";
		}
		sql = ""+table_dod+" "+table+" set "+sql+sql_dod+sql_dod2+" ";
		System.out.println("Za spremiti u bazu: "+sql);
		st = connection.createStatement();
		//st.executeUpdate(sql);
		st.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);

		int key = 0;
		if(table_id == 0)
		{
			ResultSet keys = st.getGeneratedKeys();    
			keys.next();  
			key = keys.getInt(1);
		}
		else 
		{
			key = table_id;
		}
		
		sql = " update "+table+" set orderby = ? where id = ? ";
		ps = (PreparedStatement) connection.prepareStatement(sql);
		ps.setInt(1, key);
		ps.setInt(2, key);
		ps.executeUpdate();
		
		return key;
	}
	
	public LinkedHashMap<String,String> get_data(String table, int table_id) throws SQLException{
		
		content = "";
		ResultSet rs;
		
		LinkedHashMap<String,String> map = new LinkedHashMap();
		sql = "SELECT * FROM "+table+" where id = ? ";
		System.out.println(sql);
		ps = (PreparedStatement) connection.prepareStatement(sql);
		ps.setInt(1, table_id);
		rs = ps.executeQuery();
		
		ResultSetMetaData rsMetaData = rs.getMetaData();
        int count = rsMetaData.getColumnCount();
        int j = 1;
        String cnt;
    	int m = 0;
    	while (rs.next()) {
		  //String ids = resultSet.getString(i);
	      for (int i = 1; i <= count; i++) {
            String val = rs.getString(i);
            //System.out.println(j+": "+val);
            map.put(rsMetaData.getColumnLabel(i), val);
            j++;
	      }
	      m++;
	    }
    	cnt = ""+m;
    	map.put("count", cnt);
        
    	return map;
	}
	
	public LinkedHashMap<String,Object> get_data_all(String table) throws SQLException{
		
		content = "";
		ResultSet rs;
		
		LinkedHashMap<String,Object> map = new LinkedHashMap();
		sql = "SELECT *, DATE_FORMAT(created,'%d.%m.%Y. %H:%m') as created2 FROM "+table+" order by id desc ";
		//System.out.println(sql);
		ps = (PreparedStatement) connection.prepareStatement(sql);
		//System.out.println(ps);
		rs = ps.executeQuery();
		
		String cat_title = null;
        int j = 1;
    	while (rs.next()) {
    		
    		sql = "SELECT title_hr FROM categories where id = ? order by id desc ";
    		//System.out.println(sql);
    		ps = (PreparedStatement) connection.prepareStatement(sql);
    		ps.setInt(1, rs.getInt("categories_id"));
    		ResultSet rs2 = ps.executeQuery();
    		//System.out.println(ps);
    		cat_title = "";
    		if (rs2.next()) {
    		    cat_title = rs2.getString("title_hr");
    		}
            map.put("id_"+j, Integer.toString(rs.getInt("id")));
            map.put("title_hr_"+j, rs.getString("title_hr"));
            map.put("cat_title_"+j, cat_title);
            //map.put("cat_title_"+j, Integer.toString(rs.getInt("categories_id")));
            map.put("created_date_"+j, rs.getString("created2"));
            j++;
	    }
        
    	return map;
	}
	
	
	public int get_user_created(String table, String ip) throws SQLException{
		
		content = "";
		ResultSet rs = null;
		
		sql = "SELECT TIMESTAMPDIFF(HOUR,created,now()) as hrs FROM "+table+" where ip = ? order by id desc limit 1 ";
		//System.out.println(sql);
		ps = (PreparedStatement) connection.prepareStatement(sql);
		ps.setString(1, ip);
		ResultSet rs2 = ps.executeQuery();
		//System.out.println(ps);
		String hrs = "1"; // default 1, bit ce setirano na 0 ako postoji zapis sa ip-a unatrag zadnjih sat vremena
		if (rs2.next()) {
			hrs = rs2.getString("hrs");
		}
		int hrs_val = Integer.parseInt(hrs);
		return hrs_val;
	}
	
	public boolean get_user_exists(String table, String email) throws SQLException{
		
		content = "";
		ResultSet rs = null;
		
		sql = "SELECT id FROM "+table+" where email = ? order by id desc limit 1 ";
		//System.out.println(sql);
		ps = (PreparedStatement) connection.prepareStatement(sql);
		ps.setString(1, email);
		ResultSet rs2 = ps.executeQuery();
		//System.out.println(ps);
		boolean email_exists = false;
		int em_id = 0;
		if (rs2.next()) {
			em_id = rs2.getInt("id");
		}
		if(em_id > 0){
			return true;
		} 
		else 
		{
			return false;
		}
	}
	
	public boolean delete_data(String table, int table_id) throws SQLException{
		
		content = "";
		ResultSet rs;
		
		sql = "DELETE FROM "+table+" where id = ? ";
		ps = (PreparedStatement) connection.prepareStatement(sql);
		ps.setInt(1, table_id);
		//System.out.println(ps);
		ps.executeUpdate();

    	return true;
	}
}
