package com.dev.user.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.dev.user.config.Config;
import com.dev.user.config.LogIt;
import com.dev.user.dao.DatabaseManager;

@ManagedBean(name="item")
@ViewScoped
public class Items implements Serializable  {
	
	private Config conf;
	private DatabaseManager db;
	private Connection connection;
	
	private PreparedStatement ps;
	private Statement st;
	private String sql;
	private String table;
    private int table_id;
    
	private int id;
	private long userID;
	private String first_name;
	private String last_name;
	private String email;
	private int success;
	private LinkedHashMap<String, String> map;
	private LogIt lg = new LogIt();

	public Items()
	{
		conf = new Config();
		db = new DatabaseManager();
        connection = db.getConnection();

        //setSuccess("");
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext ec = context.getExternalContext();
        String sc = (String) ec.getRequestParameterMap().get("success");
        if(sc == null){
        	sc = "0";
        }
        //this.success = Integer.parseInt(sc);
        setSuccess(Integer.parseInt(sc));
        
        String idc = (String) ec.getRequestParameterMap().get("id");
        if(idc == null){
        	idc = "0";
        }
        this.id = Integer.parseInt(idc);
        //System.out.println("idc: "+idc);

        lg = new LogIt();
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public LinkedHashMap<String, String> getMap() {
		return map;
	}

	public void setMap(LinkedHashMap<String, String> map) {
		this.map = map;
	}
	
	public int getSuccess() {
		return success;
	}

	public void setSuccess(int success) {
		this.success = success;
	}

	public int save(int redirect) throws SQLException, UnsupportedEncodingException
	{
		sql = "";
		String first = "";
		String key = "";
		String zar_dod;
		int n = 0;
		int n2 = 0;
		String viewId = "";
		String uri = "";
		String get_id = "";
		String ime_form = "";
		String prezime_form = "";
		String email_form = "";
		String ip_address = "";
		try {
			ip_address = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Map<String, String> parameterMap = (Map<String, String>) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		for (Map.Entry entry : parameterMap.entrySet()) {
		    //System.out.println("input: "+entry.getKey() + ", vrijednost: " + entry.getValue());
		    
		    if (!entry.getKey().toString().contains("e_id") && !entry.getKey().toString().contains("site_photos") &&  
    		 !entry.getKey().toString().contains("file_table") && !entry.getKey().toString().contains("j_") && 
    		 !entry.getKey().toString().contains("javax.faces.ViewState")) 
		    {
		    	first = (n2 == 0) ? entry.getKey().toString() : first;
		    	if(n2 > 0)
		    	{
			    	key = (entry.getKey().toString().contains("users_list:")) ? entry.getKey().toString().replace("users_list:","") : entry.getKey().toString();
			    	//zar_dod = (n > 0) ? ", " : "";
			    	if(!key.equals("users_list")){
				    	zar_dod = (n == 0) ? "" : ", ";
				    	sql += ""+zar_dod+""+key+"='"+entry.getValue()+"'";
				    	n++;
			    	}
		    	}
		    	n2++;
		    }
		    if (entry.getKey().toString().contains("j_") && (entry.getValue().equals("Spremi") || entry.getValue().equals("Spremi i pregledaj sve")) )
		    {
		    	viewId = FacesContext.getCurrentInstance().getViewRoot().getViewId();
		    	//System.out.println("view id: "+viewId);
		    	uri = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRequestURI();
		    	//System.out.println("uri: "+uri);
		    }
		    
		    if(entry.getKey().toString().contains("first_name")){
		    	ime_form = entry.getValue().toString();
		    }
		    if(entry.getKey().toString().contains("last_name")){
		    	prezime_form = entry.getValue().toString();
		    }
		    if(entry.getKey().toString().contains("email")){
		    	email_form = entry.getValue().toString();
		    }
		}

    	HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
    	String server_name = request.getServerName();
    	String location = "";
    	location = server_name.equals("localhost") ? "myfolderpath_files_local" : "myfolderpath_files";
    	FacesContext ctx = FacesContext.getCurrentInstance();
    	String my_dir = ctx.getExternalContext().getInitParameter(location);
    	
        //fh = new FileHandler("/home/webplace/web-place.info/site_files/my_log.log", true);  
    	//lg.logIt(my_dir+"/my_log.log", "SQL ispis: ", sql);
    	String email_from_json = "";
    	String message_content = "";
    	try {
	    	URL url = new URL("http://jsonplaceholder.typicode.com/users?email="+email_form+"");
			BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
	
			JSONParser parser=new JSONParser();
			Object object = null;
			object = parser.parse(in);
			
			JSONArray array = (JSONArray) object;        
			JSONObject object2 = (JSONObject)array.get(0);
			/*System.out.println("id: "+object2.get("id")); 
			System.out.println("name: "+object2.get("name")); 
			System.out.println("username: "+object2.get("username")); 
			System.out.println("email: "+object2.get("email"));*/
			email_from_json = object2.get("email").toString();
			
			JSONObject object3 = (JSONObject) object2.get("address");
			/*System.out.println("street: "+object3.get("street"));
			System.out.println("suite: "+object3.get("suite"));
			System.out.println("city: "+object3.get("city"));
			System.out.println("zipcode: "+object3.get("zipcode"));*/
			
			JSONObject object4 = (JSONObject) object3.get("geo");
			/*System.out.println("lat: "+object4.get("lat"));
			System.out.println("lng: "+object4.get("lng"));
			
			System.out.println("phone: "+object2.get("phone"));
			System.out.println("website: "+object2.get("website"));*/
			
			JSONObject object5 = (JSONObject) object2.get("company");
			/*System.out.println("company name: "+object5.get("name"));
			System.out.println("catchPhrase: "+object5.get("catchPhrase"));
			System.out.println("bs: "+object5.get("bs"));*/

			sql += ", email='"+email_from_json+"', "
					+ "username='"+object2.get("username")+"', street='"+object3.get("street")+"' "
					+ ", suite='"+object3.get("suite")+"', city='"+object3.get("city")+"' "
					+ ", zipcode='"+object3.get("zipcode")+"', lat='"+object4.get("lat")+"' "
					+ ", lng='"+object4.get("lng")+"', phone='"+object2.get("phone")+"' "
					+ ", website='"+object2.get("website")+"', company='"+object5.get("name")+"' "
					+ ", company_catch_phrase='"+object5.get("catchPhrase")+"', company_bs='"+object5.get("bs")+"' "
					+ ", ip='"+ip_address+"' ";
			
			/*sql += ", "
				+ "username='"+object2.get("username")+"', street='"+object3.get("street")+"' "
				+ ", suite='"+object3.get("suite")+"', city='"+object3.get("city")+"' "
				+ ", zipcode='"+object3.get("zipcode")+"', lat='"+object4.get("lat")+"' "
				+ ", lng='"+object4.get("lng")+"', phone='"+object2.get("phone")+"' "
				+ ", website='"+object2.get("website")+"', company='"+object5.get("name")+"' "
				+ ", company_catch_phrase='"+object5.get("catchPhrase")+"', company_bs='"+object5.get("bs")+"' "
				+ ", ip='"+ip_address+"' ";*/ // lokalno sql
			
			message_content = "Podaci o korisniku:\n\n"
            		+ "E-mail: "+email_form+"\n"
            		+ "Ime: "+ime_form+"\n"
            		+ "Prezime: "+prezime_form+"\n\n"
            		+ "Korisničko ime: "+object2.get("username")+"\n"
            		+ "Ulica: "+object3.get("street")+"\n"
            		+ "Apartman: "+object3.get("suite")+"\n"
            		+ "Grad: "+object3.get("city")+"\n"
            		+ "Broj pošte: "+object3.get("zipcode")+"\n"
            		+ "Koordinata (latitude): "+object4.get("lat")+"\n"
            		+ "Koordinata (langitude): "+object4.get("lng")+"\n"
            		+ "Telefon: "+object2.get("phone")+"\n"
            		+ "Website: "+object2.get("website")+"\n"
            		+ "Poslovni subjekt: "+object5.get("name")+"\n"
            		+ "Poslovni subjekt fraza: "+object5.get("catchPhrase")+"\n"
            		+ "Poslovni subjekt bs: "+object5.get("bs")+"\n"
            		;
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (org.json.simple.parser.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	try {
    		
    		if( (email_form.length() > 0) && (email_from_json.equals(email_form)) )  // ako se poslani e-mail nalazi i u json dokumentu podaci se spremaju u bazu i salju e-mailom
    		{ 
        		int hours = db.get_user_created("users_list", ip_address);
        		System.out.println("Broja sati od zadnjeg upisa: "+hours);
        		
        		if(db.get_user_exists("users_list", email_form) == true) // ako korisnik već postoji, baci statusnu poruku	
        		{
        			setSuccess(-3);
        		}
    			else if(hours == 0) // ako je sa iste ip adrese zapisivano zadnjih sat vremena, baci statusnu poruku
        		{
        			setSuccess(-2);
        		}
    			else 
    			{
	    			// ako se poslani e-mail nalazi i u json dokumentu podaci se spremaju
		    		table_id = db.save_items("users_list", id, sql); // spremanje u tablicu
		    		
		    		Properties props = new Properties();
		            Session session = Session.getDefaultInstance(props, null);
	
		            String msgBody = message_content;
	
		            try {
		            	String encodingOptions = "text/html; charset=UTF-8";
		                Message msg = new MimeMessage(session);
		                msg.setHeader("Content-Type", encodingOptions);
		                msg.setFrom(new javax.mail.internet.InternetAddress("testtest@net.hr", "Web servis - Informacije o korisniku","UTF-8"));
		                msg.addRecipient(Message.RecipientType.TO, new InternetAddress(email_form, ime_form+" "+prezime_form,"UTF-8"));
		                msg.setSubject(MimeUtility.encodeText("Informacije o korisniku", "UTF-8", "Q"));
		                //msg.setText(msgBody);
		                msg.setContent(msgBody, "text/plain; charset=UTF-8");
		                Transport.send(msg);
		                
		                //System.out.println("E-mail poruka je uspješno poslana!");
		                
		                //setSuccess(1);
		                try {
		    				FacesContext.getCurrentInstance().getExternalContext().redirect(uri+"?success=1");
		    			} catch (IOException e) {
		    				// TODO Auto-generated catch block
		    				e.printStackTrace();
		    			}
	
		            } catch (AddressException e) {
		            	e.printStackTrace();
		            } catch (MessagingException e) {
		            	e.printStackTrace();
		            } catch (NullPointerException e){
		        		e.printStackTrace();
		        	}
    			}
	    	} 
	    	else 
	    	{
	    		setSuccess(-1);
	    	}
    	} catch (NullPointerException e){
    		e.printStackTrace();
    	} catch (IOException e) {
			e.printStackTrace();
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
		} 

		return table_id;
	}
}
