package com.dev.user.config;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

public class LogIt {

	public void logIt(String filename, String msg_prefix, String msg){
		Logger logger = Logger.getLogger("MyLog");  
	    FileHandler fh;  
	
	    try {  
	        // This block configure the logger with handler and formatter  
	        //fh = new FileHandler("/home/webplace/web-place.info/site_files/my_log.log", true);  
	        fh = new FileHandler(filename, true);  
	        logger.addHandler(fh);
	        SimpleFormatter formatter = new SimpleFormatter();  
	        fh.setFormatter(formatter);  
	
	        // the following statement is used to log any messages  
	        logger.info(msg_prefix+msg);  
	
	    } catch (SecurityException e) {  
	        e.printStackTrace();  
	    } catch (IOException e) {  
	        e.printStackTrace();  
	    }  
	}
}
